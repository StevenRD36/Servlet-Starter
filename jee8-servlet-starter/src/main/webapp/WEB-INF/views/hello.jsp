<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <c:if test="${not empty addresses}">
            <table>
                <tr>
                    <th>Street</th>
                    <th>Number</th>
                    <th>City</th>
                    <th>Postcode</th>
                </tr>
                <c:forEach items="${addresses}" var="address">
                    <tr>
                        <td>${address.street}</td>
                        <td>${address.number}</td>
                        <td>${address.city}</td>
                        <td>${address.postalCode}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </body>
</html>
