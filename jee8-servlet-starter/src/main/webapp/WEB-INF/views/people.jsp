<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>People</title>
    <script type="text/javascript" src="resources/jquery-2.2.1/jquery.js"></script>
    <script type="text/javascript" src="resources/bootstrap-3.3.6/js/bootstrap.js"></script>
    <link type="text/css" rel="stylesheet" href="resources/bootstrap-3.3.6/css/bootstrap-jsf.css"/>
</head>
<body>
<div class="container">

    <!-- TODO Add a header to the page -->
    <h1>Welcome people of tomorrow, today</h1>
    <!-- TODO What if there are no people? -->
    <c:if test="${not empty people}">
        <table class="table table-striped">
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Street</th>
                <th>Number</th>
                <th>City</th>
                <th>Postcode</th>
                <th>Date of birth</th>
                <th>age</th>
            </tr>
            <c:forEach items="${people}" var="person">
                <tr>
                    <td>${person.firstName}</td>
                    <td>${person.lastName}</td>
                    <td>${person.address.street}</td>
                    <td>${person.address.number}</td>
                    <td>${person.address.city}</td>
                    <td>${person.address.postalCode}</td>
                    <td>${person.birthDate}</td>
                    <td>${person.age}</td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    
    <div>
        <form action="people" method="POST">
            <div class="form-group col-lg-4">
               <label for="firstname">First name</label>
               <input type="text" class="form-control" id="firstname" name="firstname" placeholder="enter first name"/>
            </div>
            <div class="form-group col-lg-4">
               <label for="lastname">Last name</label>
               <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Enter last name"/>
            </div>
             <div class="form-group col-lg-4">
               <label for="date">Date of birth</label>
               <input type="date" class="form-control" id="date" name="date"/>
            </div>
            <div class="form-group col-lg-8">
               <label for="street">Street</label>
               <input type="text" class="form-control" id="street" name="street" placeholder="Street"/>
            </div>
            <div class="form-group col-lg-4">
               <label for="number">Number</label>
               <input type="text" class="form-control" id="number" name="number" placeholder="number"/>
            </div>
            <div class="form-group col-lg-8">
               <label for="city">City</label>
               <input type="text" class="form-control" id="city" name="city" placeholder="city"/>
            </div>
            <div class="form-group col-lg-4">
               <label for="postalcode">Postcode</label>
               <input type="text" class="form-control" id="postcode" name="postcode" placeholder="postcode"/>
            </div>

            <input type="submit" class="btn btn-success"/>
            <input type="reset" class="btn btn-danger"/>
        </form>
    </div>

</div>
<script>
    var $pageContent = $('body > div.container');
    $pageContent.hide();
    $(function () {
        $pageContent.fadeIn();
    });
</script>
</body>
</html>
