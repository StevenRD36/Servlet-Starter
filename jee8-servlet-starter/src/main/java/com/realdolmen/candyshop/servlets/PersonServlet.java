package com.realdolmen.candyshop.servlets;

import com.realdolmen.candyshop.domain.Address;
import com.realdolmen.candyshop.domain.AddressBuilder;
import com.realdolmen.candyshop.domain.Person;
import com.realdolmen.candyshop.domain.PersonBuilder;
import com.realdolmen.candyshop.util.DateUtils;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SDOAX36
 */
@WebServlet(urlPatterns = "/people")
public class PersonServlet extends HttpServlet{
    
    private List<Person> people = new ArrayList<>(Arrays.asList(
            new PersonBuilder()
                    .setFirstName("Steven")
                    .setLastName("De Cock")
                    .setBirthDate(LocalDate.of(1987, Month.JANUARY, 8))
                    .setAddress(new Address("highstreet","1","City of angels","9000"))
                    .build(),
            new PersonBuilder().setFirstName("Winnie").setLastName("The Pooh").setBirthDate(LocalDate.of(1987, Month.JANUARY, 8)).setAddress(new Address("highstreet","1","City of angels","9000")).build(),
            new PersonBuilder().setFirstName("Winnie").setLastName("The Whoo").setBirthDate(LocalDate.of(1987, Month.JANUARY, 8)).setAddress(new Address("highstreet","1","City of angels","9000")).build(),
            new PersonBuilder().setFirstName("S.").setLastName("The Cock").setBirthDate(LocalDate.of(1987, Month.JANUARY, 8)).setAddress(new Address("highstreet","1","City of angels","9000")).build(),
            new PersonBuilder().setFirstName("Steven").setLastName("De Cock").setBirthDate(LocalDate.of(1987, Month.JANUARY, 8)).setAddress(new Address("highstreet","1","City of angels","9000")).build()
    ));

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                String address = "/WEB-INF/views/people.jsp";
        req.setAttribute("people", people);
        RequestDispatcher dispatcher = req.getRequestDispatcher(address);
        dispatcher.forward(req, resp); 
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
          System.out.println("In POST");
          String address = "/WEB-INF/views/people.jsp";
          String firstName = req.getParameter("firstname");
          String lastName = req.getParameter("lastname");
          String street = req.getParameter("street");
          String number = req.getParameter("number");
          String city = req.getParameter("city");
          String postcode = req.getParameter("postcode");
          String date = req.getParameter("date");
          
          people.add(new PersonBuilder()
                  .setFirstName(firstName)
                  .setLastName(lastName)
                  .setBirthDate(DateUtils.createDate(date))
                  .setAddress(new AddressBuilder()
                          .setStreet(street)
                          .setNumber(number)
                          .setCity(city)
                          .setPostalCode(postcode)
                          .build())
                  .build());
          req.setAttribute("people", people);
          RequestDispatcher dispatcher = req.getRequestDispatcher(address);
          dispatcher.forward(req, resp); 

    }
    
    
    
    
    
}
