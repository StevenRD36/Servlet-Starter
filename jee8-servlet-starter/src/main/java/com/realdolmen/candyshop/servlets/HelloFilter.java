package com.realdolmen.candyshop.servlets;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 *
 * @author SDOAX36
 */
@WebFilter("/hello")
public class HelloFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("Init is called");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String name = request.getParameter("name");
        if("steven".equals(name)){
            System.out.println("It's ok");
            chain.doFilter(request, response);
        }else{
            System.out.println("Not ok");
            response.getWriter().append("<h1>Wrong "+name+"</h1>");
        }
    }

    @Override
    public void destroy() {
        System.out.println("Destroy is called");
    }
    
}
