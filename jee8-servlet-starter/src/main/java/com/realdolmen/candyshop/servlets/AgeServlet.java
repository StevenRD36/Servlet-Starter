package com.realdolmen.candyshop.servlets;

import com.realdolmen.candyshop.domain.Address;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SDOAX36
 */
@WebServlet(urlPatterns = "/hello")
public class AgeServlet extends HttpServlet{

    private List<Address> addresses = new ArrayList<>(Arrays.asList(
    new Address("hoogstraat", "1", "Dorp", "1000"),
    new Address("kerkstraat","2","Stad","2000"),
    new Address("markt","3","Gehucht","3000")
    ));
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String address = "/WEB-INF/views/hello.jsp";
        req.setAttribute("addresses", addresses);
        RequestDispatcher dispatcher = req.getRequestDispatcher(address);
        dispatcher.forward(req, resp); 
    }   
}
