package com.realdolmen.candyshop.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public abstract class DateUtils {
    public static LocalDate createDate(String date) {
        System.out.println(date+" in createdate");
        if(date!=null&&!date.isEmpty()){
            return LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        return LocalDate.MIN;

    }

    public static long yearsFrom(LocalDate date) {
        return ChronoUnit.YEARS.between(date, LocalDate.now());
    }

}
