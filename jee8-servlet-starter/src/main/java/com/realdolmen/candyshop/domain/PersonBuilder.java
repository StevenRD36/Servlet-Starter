
package com.realdolmen.candyshop.domain;
import java.time.LocalDate;

public class PersonBuilder {
    
    private Person person;
    
    public PersonBuilder(){
        person = new Person();
    }
    
    public PersonBuilder setFirstName(String firstName) {
        if(firstName!=null&&!firstName.trim().isEmpty())
        {
            person.setFirstName(firstName.trim());
        }
        return this;
    }

    public PersonBuilder setLastName(String lastName) {
        if(lastName!=null&&!lastName.trim().isEmpty())
        {
            person.setLastName(lastName);
        }
        return this;
    }


    public PersonBuilder setBirthDate(LocalDate birthDate) {
        if(birthDate!=null){
            person.setBirthDate(birthDate);
        }
        return this;
    }


    public PersonBuilder setAddress(Address address) {
        if(address!=null){
            person.setAddress(address);
        }
        return this;
    }
    
    public Person build(){
        return person;
    }
}
