
package com.realdolmen.candyshop.domain;

/**
 *
 * @author SDOAX36
 */
public class AddressBuilder {
    
    private Address address;

    public AddressBuilder() {
        this.address = new Address();
    }
    
    public AddressBuilder setStreet(String street){
        if(street!=null&&!street.trim().isEmpty())
        this.address.setStreet(street);
        return this;
    }
    
    public AddressBuilder setNumber(String number){
        if(number!=null&&!number.trim().isEmpty())
        {       
            this.address.setNumber(number);
        }
        return this;
    }
    
    public AddressBuilder setCity(String city){
        if(city!=null&&!city.trim().isEmpty()){       
            this.address.setCity(city);
        }
        return this;
    }
    
    public AddressBuilder setPostalCode(String pc){
        if(pc!=null&&!pc.trim().isEmpty()){
           this.address.setPostalCode(pc);
        }
        return this;
    }
    
    public Address build(){
        return address;
    }
}
