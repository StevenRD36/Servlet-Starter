package com.realdolmen.candyshop.domain;

public class OrderLine {

    private Long id;

    private int quantity;

    private Candy candy;

    public Long getId() {
        return id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Candy getCandy() {
        return candy;
    }

    public void setCandy(Candy candy) {
        this.candy = candy;
    }

    public double calculateSubTotal() {
        return candy.getPrice() * quantity;
    }
}
