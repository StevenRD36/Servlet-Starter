package com.realdolmen.candyshop.domain;

public class GummyBears extends Candy {
    private String flavour;

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }
}
