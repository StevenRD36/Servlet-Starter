package com.realdolmen.candyshop.domain;

import com.realdolmen.candyshop.util.DateUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Person {

    private Long id;
    private String firstName;

    private String lastName;
    private LocalDate birthDate;
    private long age;

    private int version;

    private Address address;
    private List<CandyColor> candyPreferences = new ArrayList<>();
    private List<Order> orderHistory = new ArrayList<>();

    public void initializeAge() {
        this.age = DateUtils.yearsFrom(birthDate);
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        initializeAge();
    }

    public long getAge() {
        return age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<CandyColor> getCandyPreferences() {
        return candyPreferences;
    }

    public void setCandyPreferences(List<CandyColor> candyPreferences) {
        this.candyPreferences = candyPreferences;
    }

    public List<Order> getOrderHistory() {
        return Collections.unmodifiableList(this.orderHistory);
    }

    void addOrderToHistory(Order order) {
        orderHistory.add(order);
    }

    public int getVersion() {
        return version;
    }
}
