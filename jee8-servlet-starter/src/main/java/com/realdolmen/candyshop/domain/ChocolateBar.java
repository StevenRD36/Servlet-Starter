package com.realdolmen.candyshop.domain;

public class ChocolateBar extends Candy {
    private int length;

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
